package com.brijeshsonawane234.post.Api;

import com.brijeshsonawane234.post.Pojo.JSON;
import com.brijeshsonawane234.post.Pojo.PostInfo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiRetrofit
{
    @POST("homepage/homebanner")
    Call<JSON> postData(@Body PostInfo postInfo);

}
